<?php

defined('MOODLE_INTERNAL') || die();

require_once("lib.php");


class block_mabase_renderer_base extends plugin_renderer_base {
	// general code for this plugin
}

class block_mabase_renderer extends block_mabase_renderer_base {
	// customized code for this client
}